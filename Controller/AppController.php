<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');
CakePlugin::load('Export');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $helpers = array('Html', 'Form', 'Js');
    // var $components = array('Export.Export', 'Auth', 'Session', 'Cookie', 'RequestHandler', 'Security');

    // public $components = array('RequestHandler');

    public $components = array(
        'Export.Export', 
        'Session',
        'Cookie', 'RequestHandler', 
        // 'Security',
        'Auth'=>array(
            'loginRedirect'=>array('controller'=>'users','action'=>'index'),
            'logoutRedirect'=>array('controller'=>'users','action'=>'index'),
            'authError'=>"You can't access that page",
            'authorize'=>array('Controller')
        )
    );

    public function isAuthorized($user) {
        return true;
    }

    public function beforeFilter()
    {
        $this->Auth->allow('login');
        $this->set('logged_in', $this->Auth->loggedIn());
        $this->set('current_user', $this->Auth->user());
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

}
