<?php
App::uses('AppController', 'Controller', 'Folder', 'File', 'Utility');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $name  = 'Posts' ;
	public $helpers = array('Js');
	public $uses = array('Post', 'Image');
	public $components = array('Paginator','RequestHandler');
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// $this->Post->recursive = 0;
		$this->set('posts', $this->Paginator->paginate());

		$current_user = $this->Auth->user();
		if($current_user['role'] == 'admin') {
			$this->set('posts',$this->Post->find('all',array('order' => 'Post.id')));
		}
		if($current_user['role'] != 'admin') {
			$displayPost = $this->Post->find('all',array('order'=>array('Post.id' => 'asc')));
			$this->set('posts',$displayPost);
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function view($id = null) {

		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is('post')) {
			// configure::write('debug', 0);
			$this->request->data['Comment']['user_id'] = $this->Auth->user('id');
			$this->request->data['Comment']['username'] = $this->Auth->user('username');

			if(!empty($this->data)){
				if($this->Post->Comment->save($this->request->data)) {
					if($this->RequestHandler->isAjax()){
						// $this->render('success','ajax');
						$this->Session->setFlash(_('Add Comment Successfully'));
						return $this->redirect(array('action'=>'view', $id));
						}
				} else {
					$this->Session->setFlash(_('Unable to add your comment.'));
				}
			}
		}

		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$user = $this->Post->find('first', $options);
		// debug($user); exit; 
		$this->set('post', $this->Post->find('first', $options));
	}

	public function viewAjax(){
		$this->autoRender=false;
	
		Configure::write('debug',3);

		if ($this->request->is('post')) {
			$this->request->data['Comment']['user_id'] = $this->Auth->user('id');
			$this->request->data['Comment']['username'] = $this->Auth->user('username');
			// $this->request->data['Comment']['post_id'] = $this->request->data['Post']['post_id'];
			// $this->request->data['Comment']['comment'] = $this->request->data['Post']['comment'];

			try{
				$save  = $this->Post->Comment->save($this->request->data['Comment']);
			
				return json_encode($save);
				
			} catch (commentlastCreated $e)
			{
				echo $e->errorMessage();
			// 	return json_encode('error');
			}
			
		// }
		}
	}


/**
 * add method
 *
 * @return void
 */

	public function initialize(){
		parent::initialize();
		$this->loadModel('Posts');
	}

	public function add() {
		if ($this->request->is('post')) {
				$this->Post->create();
				$this->request->data['Post']['user_id'] = $this->Auth->user('id');
				$post = $this->Post->save($this->request->data);
				if ($post) {
					
				foreach($this->request->data['Post']['fileUpload'] as $name => $image)
				{
					$uploadFolder = WWW_ROOT. 'img\uploadimg'.DS.$post['Post']['id'];
					$filename = time() .'_'. $image['name']; 
					$uploadPath =  $uploadFolder . DS . $image['name'];
				
					if( !file_exists($uploadFolder) ){
						mkdir($uploadFolder); 
					}
						
						if (move_uploaded_file($image['tmp_name'], $uploadPath)) 
						{
							$this->Image->create();
							$images = $this->Image->save(array(
								//เพิ่มข้อมูลลง database
								'image_name' => $image['name'],
								'image_path' => $uploadPath,
								'created' =>  $post['Post']['created'],
								'modified' => $post['Post']['modified'],
								'post_id' =>  $post['Post']['id']
							));
						}
						
					} 
						$this->Session->setFlash(__('The post has been saved.'));
						return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
				}
			}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$post = $this->Post->save($this->request->data);
			// debug($post); exit;
			if ($post) {
				foreach($this->request->data['Post']['fileUpload'] as $name => $image)
				{
					$uploadFolder = WWW_ROOT. 'img\uploadimg'.DS.$post['Post']['id'];
					$filename = time() .'_'. $image['name']; 
					$uploadPath =  $uploadFolder . DS . $image['name'];
				
					if( !file_exists($uploadFolder) ){
						mkdir($uploadFolder); 
					}
						
						if (move_uploaded_file($image['tmp_name'], $uploadPath)) 
						{
							$this->Image->create();
							$images = $this->Image->save(array(
								//เพิ่มข้อมูลลง database
								'image_name' => $image['name'],
								'image_path' => $uploadPath,
								'created' =>  $post['Post'],
								'modified' => $post['Post']['modified'],
								'post_id' =>  $post['Post']['id']
							));
						}
						
					} 
				$this->Session->setFlash(__('The post has been saved.'));
				return $this->redirect(array('controller' => 'posts','action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}

			
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			//debug( $this->Post->find('first', $options));
			$this->request->data = $this->Post->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		App::uses('Folder', 'Utility');

		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');

		$deleteFolder = new Folder(WWW_ROOT.'img\uploadimg'.DS.$id);
		
			if($this->Post->Comment->deleteAll(array('Comment.post_id' => $id)) && 
			$this->Image->deleteAll(array('Image.post_id' => $id)) && $this->Post->delete($id)) {
				if($deleteFolder->delete()){
				}
					$this->Session->setFlash(__('The post has been deleted.'));
				} 
				else {
					$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
				}
			return $this->redirect(array('controller' => 'posts','action' => 'index'));
	}

	public function deleteImg($id = null){
		$data1 = $this->Image->findById($id);
		$imgSelect = $data1;
		$post_id = $imgSelect['Image']['post_id'];
		$imageName = $imgSelect['Image']['image_name'];
		$deletePath = new File(WWW_ROOT.'img\uploadimg'.DS.$post_id.DS.$imageName);

			if($this->Image->delete($id)){
				if($deletePath->delete()){
				}
				$this->Session->setFlash(__('The Image has been deleted.'));
		} else {
				$this->Session->setFlash(__('The Image could not be deleted. Please, try again.'));
			}
		return $this->redirect(array('controller' => 'posts', 'action' => 'edit',$post_id));
	}

	public function export_datas() {

		// It's OK to use containable or recursive in the export data
		$posts = $this->Post->find('all');
		
		foreach ($posts as $post)
		{
			$data[] = $post['Post'];
		}
		// debug($data); exit;
		$this->Export->exportCsv($data, 'posts.csv');
		// a CSV file called myExport.csv will be downloaded by the browser.
	}

	

}

?>
