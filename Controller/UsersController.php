<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

	public $name = 'Users';
	public $uses = array('User', 'Post');
	public $helpers = array('Js');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('add');
	}

	public function isAuthorized($user) {
		if ($user['role'] == 'admin') {
			return true;
		}
		if (in_array($this->action, array('edit','delete'))) {
			if ($user['id'] != $this->request->params['pass'][0]) {
				return false;
			}
		}
		return true;
	}

	public function login() {
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->session->setFlash('Your username/password combination was incorrect');
			}
		}
	}

	public function logout() 
	{
		$this->redirect($this->Auth->logout()) ;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}

		$user = $this->User->findById($id);
		$post = $this->User->Post->find('all',
				array(
					'conditions' => array('Post.user_id' => $id))
			);

		if ($this->request->is('post')) {
			$this->request->data['Comment']['user_id'] = $this->Auth->user('id');
			$this->request->data['Comment']['username'] = $this->Auth->user('username');

			$comment = $this->request->data;
			// debug($comment['Comment']); exit;
			if(isset($comment['Comment']['comment'])){
				if($this->Post->Comment->save($comment['Comment'])) {
					$this->Session->setFlash(_('Add Comment Successfully'));
					return $this->redirect(array('action'=>'view', $id));
				} else {
					$this->Session->setFlash(_('Unable to add your post.'));
				}
			}

			if($this->Post->save($comment['Post'])) {
				$this->Session->setFlash(_('Add Post Successfully'));
				return $this->redirect(array('action'=>'view', $id));
			} else {
				$this->Session->setFlash(_('Unable to add your post.'));
			}
		}

		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set(compact('post','user'));
	}

	public function viewAjax(){
		$this->autoRender=false;
	
		Configure::write('debug',3);
		debug($this->request->data);
		exit;
		if ($this->request->is('post')) {
			$this->request->data['Comment']['user_id'] = $this->Auth->user('id');
			$this->request->data['Comment']['username'] = $this->Auth->user('username');
			try{
				$save  = $this->Post->Comment->save($this->request->data['Comment']);
				$semiRandomArticle = $this->Post->Comment->find('first');
				$commentlastCreated = $this->Post->Comment->find('first', array(
					'order' => array('comment.created' => 'desc')
				));
				return json_encode($commentlastCreated);
				
			} catch (commentlastCreated $e)
			{
				echo $e->errorMessage();
			}
	}
}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			$this->request->data['Post']['user_id'] = $this->Auth->user('id');

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');

		if ($this->User->Post->deleteAll(array('Post.user_id'=> $id )) && $this->User->delete($id)) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function export_datas() {

		// It's OK to use containable or recursive in the export data
		$users = $this->User->find('all');
		
		foreach ($users as $user)
		{
			$data[] = $user['User'];
		}
		// debug($data); exit;
		$this->Export->exportCsv($data, 'users.csv');
		// a CSV file called myExport.csv will be downloaded by the browser.
		
		
		// $this->layout=false;
		// require_once 'php-export-data.class.php';
		  
		// $posts = $this->Post->find('all',array('Post.id','Post.title','Post.body','Post.created','Post.modified','Post.user_id'));

		// $exporter = new ExportDataExcel('browser', 'posts.xls');
		// $exporter->initialize();
		// $exporter->addRow(array("Id", "Title", "Body", "Created","Modified"));

		// foreach($posts as $key => $data) {
		// 	$exporter->addRow(array($data['Post']['id'], $data['Post']['title'], $data['Post']['body'], $data['Post']['created'], $data['Post']['modified'], $data['Post']['user_id']));
		// }

		// $exporter->finalize();
		// exit();
	}
}
