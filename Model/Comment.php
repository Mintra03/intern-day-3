<?php
App::uses('AppModel', 'Model');
/**
 * Comment Model
 *
 */
class Comment extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $belongsTo = array(
		'Post' => array(
			'className' => 'Post',
			'foreignKey' => 'post_id'
		)
	);

	public $validate = array(
		'comment'=>array(
            'rule'=>'notBlank',
            'required'=>true,
            'message'=>'Comment is required!'
        ),
	);
}
