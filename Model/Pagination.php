<?php
    include 'Pagination.class.php';
    $page = 1;
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
    $row = 10;

    $totaldata_query="SELECT * FROM your_table";
    $resultdata=mysqli_query($conn,$totaldata_query)or die("Query failed");
    $total_data= mysqli_num_rows($resultdata);
    $total_page = ceil($total_data/$row);
    if ($page>=$total_page) {
        $page=$total_page;
    }
    if($page<=0){
        $page=1;
    }
    $start = ($page - 1) * $row;
    $Pagination = new Pagination($page, $total_data);
    $Pagination->parse();
?>