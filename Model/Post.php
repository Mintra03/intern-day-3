<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Post extends AppModel {
    public $hasMany = array(
        'Comment'=> array(
            'order' => 'created DESC'
        ),
        'Image' => array(
            'order' => 'created ASC'
        )
    );

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        )
    );
    
    public $validate = array(
        'title'=>array(
            'rule'=>'notBlank',
            'required'=>true,
            'message'=>'Title is required!'
        ),
        'body'=>array(
            'rule'=>'notBlank',
            'required'=>true,
            'message'=>'Body is required!'
        )
    );

}
