<?php 
class User extends AppModel {
    public $name = 'User' ;
    public $displayField = 'name' ;

    public $hasMany = array(
            'Post' => array(
                'className' => 'Post',
                'foreignKey' => 'user_id',
                'order' => 'created DESC',
            ),
            'Comment'=> array(
                'className' => 'Comment',
                'foreignKey' => 'user_id',
                'order' => 'created DESC'
            )
    );

    public $validate = array(
        'name'=>array(
           'Please enter your name.'=>array(
               'rule'=>'notBlank',
               'message'=>'Please enter your name.'
           ) 
        ),
        'username'=>array(
            'The username must be between 5 and 15 characters.'=>array(
                'rule'=>array('between', 5, 15),
                'message'=>'The username must be between 5 and 15 characters.'
            ),
            'That username has already been taken'=>array(
                'rule'=>'isUnique',
                'message'=>'That username has already been taken.'
            )
        ),
        'email'=>array(
            'Valid email'=>array(
                'rule'=>array('email'),
                'message'=>'Please enter a valid email address.'
            )
        ),
        'password'=>array(
            'Not empty'=>array(
                'rule'=>'notBlank',
                'message'=>'Please enter your password'
            ),
            'Match password'=>array(
                'rule'=>'matchPasswords',
                'message'=>'Your passwords do not match'
            )
        ),
        'password_confirmation'=>array(
            'Not empty'=>array(
                'rule'=>'notBlank',
                'message'=>'Please confirm your password'
            )
        )
    );

    // public function searchComment($id) {
    //     $users = $this->findById($id);
    //     $postComment = array();

    //     foreach ($users['Post'] as $key => $post) {
    //         $comment = $this->Comment->query('SELECT users.id,comment,name From myblog.comments join users on user_id = comments.user_id where post_id = '.$post['id'].'');
    //         $post['comment'] = $comment;
    //         $postComment[] = $post;
    //     }
    //     return $postComment;
    // }

    public function matchPasswords($data) {
        if ($data['password'] == $this->data['User']['password_confirmation']) {
            return true;
        }
        $this->invalidate('password_confirmation', 'Your passwords do not match');
        return false;
    }

    public function beforeSave($id = null) {
        if (isset($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }
        return true;
    }
}
?>
