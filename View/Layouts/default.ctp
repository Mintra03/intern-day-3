<?php
/**
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- <script src="../../webroot/js/comment.js"></script> -->
	

	<?php
		
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		
		echo $this->Html->script('comment');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		echo $this->Js->writeBuffer(array('cache'=>true));
	
	?>

	<?php
		$phpFileUploadErrors = array(
			0 => 'There is no error, the file uploaded with success',
			1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
			2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
			3 => 'The uploaded file was only partially uploaded',
			4 => 'No file was uploaded',
			6 => 'Missing a temporary folder',
			7 => 'Failed to write file to disk.',
			8 => 'A PHP extension stopped the file upload.',
		);
	?>

	<style>
		body {
			background: #F5F5F5;
			color: black;
			font-family:'lucida grande',verdana,helvetica,arial,sans-serif;
			font-size:95%;
			margin: 0;
		}

		h2 {
			background:#fff0 ; 
			color: #e32;
			font-family:'Gill Sans','lucida grande', helvetica, arial, sans-serif;
			font-size: 190%;
		}

		.paging {
			background: #fff0;
			color: #ccc;
			margin-top: 1em;
			clear: both;
		}
	</style>
</head>
<body>
	<!-- <div id="container" > -->

		<!-- <div id="header">
		</div> -->
		
	<div class='row'>
		<div class="col-sm-10" style="padding-right: 0px; padding-left: 0px; " style='background:#003333;'>
			<nav class="navbar navbar-expand-sm navbar-dark justify-content-right" style='background:#003333; height: 54px;'>
			<i class="fas fa-user-circle" style='font-size:30px;color:#003333'></i>
			<ul class="navbar-nav">
				<li class="nav-item">
				<a class="nav-link" style="color:#7FFFD4; text-decoration:none; font-weight: 500; font-size: 18px; padding-left: 0px; padding-right: 0px;" href="/MyBlog/users">Users</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" style="color:#7FFFD4; text-decoration:none; font-weight: 500; font-size: 18px;" href="/MyBlog/posts">Posts</a>
				</li>
			</nav>
		</div>

		<div class="col-sm-2" style="padding-right: 0px; padding-left: 0px;" style='background:#003333;'>
			<nav class="navbar navbar-expand-sm navbar-dark justify-content-right" style='background:#003333; '>
		
		<!-- <div id="content" style="background-color:#F5F5F5;"> -->
			<!-- <div style="text-align: right;"> -->
				<?php if ($logged_in): ?>
					<h6 style="color:#7FFFD4;"> Welcome! <i class="fas fa-user-circle" style='font-size:30px;color:#FFF5EE'></i>   <strong style="color:#FA8072;"><?php echo $current_user['username']; ?></strong>. <small style="color:#7FFFD4;"><?php echo $this->Html->link('Logout', array('controller'=>'users','action'=>'logout'), array('style' => 'color:#AFEEEE;')); ?></small></h6>
				<?php else: ?>
				<a href="/MyBlog/" style="color:#AFEEEE; height: 38px;" >Login</a>		
				<?php endif; ?>
			<!-- </div> -->
			
			</nav>
		</div>
	</div>

			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Session->flash('auth'); ?>
			<?php echo $this->fetch('content'); ?>
		<!-- </div> -->

		<!-- <div id="footer">
		</div> -->

	<!-- </div> -->
</body>
</html>
