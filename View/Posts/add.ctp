<div class="posts form">
<div class="content">
	<?php echo $this->Flash->render(); ?>
<div class="upload-form">
<?php echo $this->Form->create('Post', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Post'); ?></legend>

		<?php
			echo $this->Form->input('title');
			echo $this->Form->input('body');
		?>

		<dt><?php echo __('Image'); ?></dt>
		
		<?php
			echo $this->Form->input('fileUpload.', array('type' => 'file', 'multiple', 'class'=> 'form-controlbtn')); 
		?>

		<!-- <?php echo $this->Form->button(_('upload-form'), ['type'=> 'submit', 'class' => 'form-controlbtn btn-default']); ?> -->
		<?php echo $this->Form->end(__('submit')); ?>
	</fieldset>

</div>
</div>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?></li>
	</ul>
</div>
