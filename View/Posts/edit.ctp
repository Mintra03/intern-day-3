<div class="posts form">
<?php echo $this->Form->create('Post', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit Post'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('title');
			echo $this->Form->input('body'); 
		?>
			<dt><?php echo __('Image'); ?></dt>

			<div class="input file required">
			<!-- <label for="PostFileUpload"></label> -->
			<input type="file" name="data[Post][fileUpload][]" multiple="multiple" class="form-controlbtn" id="PostFileUpload" ></div>
			<!-- <?php
			echo $this->Form->input('fileUpload.', array('type' => 'file', 'multiple', 'class'=> 'form-controlbtn'));
			?> -->
			&nbsp;
			&nbsp;

				<dd>
					<?php
					// debug($this->request->data['Image']); 
					foreach($this->request->data['Image'] as $image): ?>
					&nbsp;
					<img src="<?php echo $this->webroot; ?>img/uploadimg/<?php echo $image['post_id'] ?>/<?php echo $image['image_name'] ;?>" width='60%' height='60%' >
					<p><?php echo $image['image_name']; ?></p>
					<!-- <p align = 'center'>  -->
					<p><?php 
					// echo $this->Html->link('Download', ['action'=>'download', $file->id],['class'=>'btn btn-primary']);
					echo $this->Html->link('Delete', array('controller' => 'posts', 'action' => 'deleteImg', $image['id']),array('class'=>'btn btn-danger'),array('confirm' => __('Are you sure you want to delete # %s?')));
					// debug($image);
					?> 
					</p>
					<?php endforeach; ?>
					&nbsp;
					&nbsp;
				</dd>
		
		
		<?php echo $this->Form->end(__('Submit')); ?>
	</fieldset>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Post.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Post.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?></li>
	</ul>
</div>
