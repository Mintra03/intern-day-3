<div class="posts view">
<h2><?php echo __('Post'); ?></h2>
	
	<dl>
		<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($post['Post']['id']); ?>
				&nbsp;
			</dd>
		<dt><?php echo __('By'); ?></dt>
			<dd>
				<?php echo h($post['User']['name']); ?>
				&nbsp;
			</dd>
		<dt><?php echo __('Title'); ?></dt>
			<dd>
				<?php echo h($post['Post']['title']); ?>
				&nbsp;
			</dd>

		<dt><?php echo __('Body'); ?></dt>
			<dd>
				<?php echo h($post['Post']['body']); ?>
				&nbsp;
			</dd>
		
		<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($post['Post']['created']); ?>
				&nbsp;
			</dd>
		<dt><?php echo __('Modified'); ?></dt>
			<dd>
				<?php echo h($post['Post']['modified']); ?>
				&nbsp;
			</dd>
		<dt><?php echo __('Image'); ?></dt>
			<dd>
				<?php foreach($post['Image'] as $image): ?> 
				&nbsp;
				<img src="<?php echo $this->webroot; ?>img/uploadimg/<?php echo $image['post_id'] ?>/<?php echo $image['image_name'] ;?>" width='90%' height='90%'>
				<p><?php echo $image['image_name']; ?></p>
				<?php endforeach; ?>
				&nbsp;
				&nbsp;
			</dd>
	</dl>

	<!-- <div id="success"> </div> -->
	<!-- รับค่า comment -->
	<!-- <?php 
		echo $this->Form->create('Post',array('type' => 'post',
		'url'   => array(
			'controller' => 'posts','action' => 'viewAjax'
		),'id'=>'saveForm'));
		echo $this->Form->hidden('post_id', array('value' => $post['Post']['id']));
		echo $this->Form->input('comment',array('id'=>'CommentComment'));
		echo $this->Form->submit('Add Comment',array('id'=>'addCom'));
		echo $this->Form->end();
	?> -->

	<div id = "success"></div> 
		<form class="<?php echo $post['Post']['id']; ?>" method="POST" accept-charset="utf-8">
			<div style="display:none;">
				<input type="hidden" name="_method" value="POST">
			</div>
				<input type="hidden" id="CommentPostId" name="data[Comment][post_id]" value="<?php echo  $post['Post']['id'];?>" >
			<div class="input textarea required">
				<label for="CommentComment">Comment</label>
				<textarea name="data[Comment][comment]" id="CommentComment" required="required"></textarea>
			</div>
			<!-- <div class="submit"> -->
				<input type="button" class="saveForm" id="from-<?php echo $post['Post']['id']; ?>" value="Add Comment" >
			<!-- </div> -->
		</form>


	<div id="div1" style="heightt:60px; width:1350px; border:1px; margin-top:5px;"></div> 
	<hr>
	<!-- แสดงผล comment -->
	<?php foreach($post['Comment'] as $comment): ?>
		<p><?php echo __('By: '); ?><?php echo h($comment['username']);?></p>
		<p><?php echo h($comment['comment']);?></p>
		<p><?php echo $comment['created'];?></p>
		<!-- <?php debug($comment); ?> -->
		<hr>
	<?php endforeach; ?>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Post'), array('action' => 'edit', $post['Post']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Post'), array('controller' => 'posts','action' => 'delete', $post['Post']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('action' => 'add')); ?> </li>
	</ul>
</div>
