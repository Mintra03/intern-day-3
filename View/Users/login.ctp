		<br>
		<br>

<div class="container">
  	<div class="row" >
		<div class="col" style="background-color:#F5F5F5;"></div>
		<div style="background-color:#fff;"  class="rounded-lg">
  
			<br>
			<br>
			<br><h2 class="text-center" style="color:#003333;"> Welcome!! <small class="text-muted" >Login</small></h2>
			<br>

			<?php
			echo $this->Form->create();
			?>
			
			<div class="input text required">
				<label for="UserUsername" class="text-muted">Username:</label>
					<div class="input-group-prepend">
						<span class="input-group-text"><i class='fas fa-user' style='font-size:24px;color:#003333' style="color:#006666;"></i></span>
						<input name="data[User][username]" class="rounded-lg" maxlength="45" type="text" id="UserUsername" required="required" placeholder="Username">
					</div>
			</div>

			<div class="input password required">
				<label for="UserPassword" class="text-muted">Password:</label>
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-unlock-alt" style='font-size:24px;color:#003333' style="color:#006666;"></i></span>
						<input name="data[User][password]" class="rounded-lg" type="password" id="UserPassword" required="required" placeholder="Password">
					</div>
			</div>

			<div class="text-center">
				<button type="submit" class="btn btn-primary" style="background-color:#006666;">
				<!-- <span class="spinner-border spinner-border-sm"></span> -->
				Login
				</button>
			</div>
			
		</div>
    	<div class="col" style="background-color:#F5F5F5;"></div>
	</div>
</div>