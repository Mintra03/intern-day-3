<div class="users view">
<h2 ><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
	</dl>

	
		<?php echo $this->Form->create('Post'); 
			// false,
			// array('url'=>array('controller'=>'posts','action'=>'add'),
			// 'id' => 'post')); ?>

			<fieldset>
				<legend><?php echo __('Add Post'); ?></legend>
			<?php
				echo $this->Form->input('title');
				echo $this->Form->input('body');
				echo $this->Form->hidden('user_id', array('value' => $user['User']['id']));
			?>
			</fieldset>
		<?php echo $this->Form->end(__('Submit'), array('controller' => 'users', 'action' => 'add')); ?>

	<?php foreach($post as $posts): ?>
	<h2 ><?php echo __('Post'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($posts['Post']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($posts['Post']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($posts['Post']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Create'); ?></dt>
		<dd>
			<?php echo h($posts['Post']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($posts['Post']['modified']); ?>
			&nbsp;
		</dd>

		<div id="success"> </div>
		<!-- รับค่า comment -->
		<!-- <?php 
		echo $this->Form->create('Post',array('type' => 'post',
		'id'=> $posts['Post']['id']));
		echo $this->Form->hidden('post_id', array('value' => $posts['Post']['id']));
		echo $this->Form->input('comment',array('id'=>'CommentComment'));
		echo $this->Form->submit('Add Comment',array('id'=>'addCom'));
		echo $this->Form->end();
	?> -->

	<div id = "success"></div> 
			<form  class="<?php echo $posts['Post']['id']; ?>" method="POST" accept-charset="utf-8">
				<div style="display:none;">
					<input type="hidden" name="_method" value="POST">
				</div>
					<input type="hidden" id="CommentPostId" name="data[Comment][post_id]" value="<?php echo  $posts['Post']['id'];?>" >
				<div class="input textarea required">
					<label for="CommentComment">Comment</label>
					<textarea name="data[Comment][comment]" id="CommentComment" required="required"></textarea>
				</div>
				<!-- <div class="submit"> -->
					<input type="button" class="saveForm" id="from-<?php echo $posts['Post']['id']; ?>"  value="Add Comment" >
				<!-- </div> -->
	</form>
			
		<div id="from-<?php echo $posts['Post']['id']; ?>"  >
			<!-- แสดงผล comment -->
				<?php foreach($posts['Comment'] as $comment): ?>
					<p><?php echo __('By: '); ?><?php echo h($comment['username']);?></p>
					<p><?php echo h($comment['comment']);?></p>
					<p><?php echo $comment['created'];?></p>
					<hr>
				<?php endforeach; ?>
		</div>
	</dl>
	<?php endforeach; ?>

	
	<!-- <?php debug($current_user);?> -->

</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<?php if ($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'): ?>
			<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?> </li>
		<?php endif; ?>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
